﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Agent : MonoBehaviour
{
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] Transform target;

    void Update()
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(target.position, out hit, Mathf.Infinity, NavMesh.AllAreas);

        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(this.transform.position, hit.position, NavMesh.AllAreas, path);

        Vector3 nextPosition = this.transform.position;

        for (int i = 0; i < path.corners.Length; i++)
        {
            if (i > 0)
                Debug.DrawLine(path.corners[i - 1], path.corners[i], Color.red);
        }

        for (int i = 0; i < path.corners.Length; i++)
        {
            if ((path.corners[i] - path.corners[0]).magnitude > 0.5f)
            {
                nextPosition = path.corners[i];
                break;
            }
            nextPosition = path.corners[i];
        }

        Vector3 velocity = (nextPosition - this.transform.position).normalized * 3f;
        velocity.y = rigidbody.velocity.y;
        rigidbody.velocity = velocity;
    }
}
