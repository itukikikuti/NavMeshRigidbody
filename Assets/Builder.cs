﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Builder : MonoBehaviour
{
    [SerializeField] Transform target; // この座標中心にNavMeshを生成する
    [SerializeField] Vector3 size; // NavMeshを生成する範囲
    [SerializeField] MeshFilter[] meshes;

    IEnumerator Start()
    {
        NavMeshData data = new NavMeshData();
        NavMesh.AddNavMeshData(data);

        while (true)
        {
            NavMeshBuildSettings settings = NavMesh.GetSettingsByID(0);
            Bounds bounds = new Bounds(target.position, size);

            List<NavMeshBuildSource> sources = new List<NavMeshBuildSource>();
            foreach (var mesh in meshes)
            {
                NavMeshBuildSource source = new NavMeshBuildSource();
                source.shape = NavMeshBuildSourceShape.Mesh;
                source.sourceObject = mesh.sharedMesh;
                source.transform = mesh.transform.localToWorldMatrix;
                source.area = 0;

                sources.Add(source);
            }

            NavMeshBuilder.UpdateNavMeshData(data, settings, sources, bounds);

            yield return new WaitForSeconds(1f);
        }
    }
}
